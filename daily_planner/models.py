from django.db import models
from django.utils import timezone
import datetime


class Goal(models.Model):
    goal_text = models.CharField(max_length=300)
    goal_deadline = models.DateTimeField('Deadline')
    def __str__(self):
        return self.goal_text

    def was_published_recently(self):
        return self.goal_deadline >= timezone.now() - datetime.timedelta(days=1)

class Note(models.Model):
    note_text = models.CharField(max_length=300)
    def __str__(self):
        return self.note_text

class Importance(models.Model):
    goal = models.ForeignKey(Goal, on_delete = models.CASCADE)
    importance_text=models.CharField(max_length=10)
    def __str__(self):
        return self.importance_text
