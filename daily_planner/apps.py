from django.apps import AppConfig


class DailyPlannerConfig(AppConfig):
    name = 'daily_planner'
