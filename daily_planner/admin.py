from django.contrib import admin

from .models import Goal,Note,Importance


class ImportanceInline(admin.StackedInline):
    model = Importance
    extra = 3

class GoalAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['goal_text']}),
        ('Deadline', {'fields':['goal_deadline'], 'classes': ['collapse']}),
    ]
    inlines = [ImportanceInline]

admin.site.register(Goal, GoalAdmin)

