from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Goal,Importance

class IndexView(generic.ListView):
    template_name = 'daily_planner/index.html'
    context_object_name = 'latest_goals_list'

    def get_queryset(self):
        return Goal.objects.order_by('-goal_deadline')

class DetailView(generic.DetailView):
    model = Goal
    template_name = 'daily_planner/results.html'

    def get_queryset(self):
        return Goal.objects.filter(goal_deadline__lte=timezone.now())

class ResultsView(generic.DetailView):
    model = Goal
    template_name = 'daily_planner/results.html'

def vote(request,goal_id):
    goal = get_object_or_404(Goal, pk=goal_id)
    try:
        selected_importance = goal.importance_set.get(pk=request.POST['importance'])
    except (KeyError, Importance.DoesNotExist):
        return render(request, 'daily_planner/detail.html', {
            'goal':goal,
            'error_message': "You didn't did this",
        })
    else:
        selected_importance.save()
        return HttpResponseRedirect(reverse('daily_planner:results', args=(goal.id, )))

